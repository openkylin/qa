# 开放麒麟(openKylin)-0.9 版本公测-测试方案	

## 版本情况	

### 版本内容	

 1.新增互联互通、UKUI、SDK等多个需求（详见[0.9版本发行说明](https://gitee.com/openkylin/release-management/blob/master/openKylin-0.9/openKylin-0.9%E7%89%88%E6%9C%AC%E5%8F%91%E8%A1%8C%E8%AF%B4%E6%98%8E.md)）

 2.修复issue（详见版本发布说明）

## 测试计划

### 测试环境

 1.X86、RISC-V、虚拟机（Linux\win+VMware、virtualbox）

 2.开发板适配测试及特殊机型缺陷验证需要使用指定设备，其他无要求
 
 3.系统支持PC平板二合一，可在PC平板两种模式下进行验证

### 测试策略

1.版本测试，对于新增功能、回归issue的针对性测试

2.虚拟机支持：支持Linux\win+VMware、virtualbox及增强功能等


### 测试时间

11月4日--10月21日

### 测试内容

1.新增内容试用测试：互联互通、UKUI、SDK等多个需求（详见[0.9版本发行说明](https://gitee.com/openkylin/release-management/blob/master/openKylin-0.9/openKylin-0.9%E7%89%88%E6%9C%AC%E5%8F%91%E8%A1%8C%E8%AF%B4%E6%98%8E.md)）

2.版本已解决issue验证，重点验证issue解决及是否有新问题引入（issue列表详见[0.9版本发行说明](https://gitee.com/openkylin/release-management/blob/master/openKylin-0.9/openKylin-0.9%E7%89%88%E6%9C%AC%E5%8F%91%E8%A1%8C%E8%AF%B4%E6%98%8E.md)）

### issue提交
规范可参考
[openKylin测试参与指南](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%89%88%E6%9C%AC%E6%B5%8B%E8%AF%95%E5%8F%82%E4%B8%8E%E6%8C%87%E5%8D%97.md) 及
[openKylin 缺陷issue处理流程](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%BC%BA%E9%99%B7%E5%A4%84%E7%90%86%E6%B5%81%E7%A8%8B.md)

1.所有[已知issue](https://gitee.com/organizations/openkylin/issues?assignee_id=&author_id=&branch=&collaborator_ids=&issue_search=&label_ids=&label_text=&milestone_id=&priority=&private_issue=&program_id=&project_id=&project_type=&scope=&single_label_id=&single_label_text=&sort=&state=progressing&target_project=) 可见 gitee-openKylin-【任务】-“进行中”，以及QA仓库历史已知问题列表，已知缺陷不需要重复提交；

2.issue提交在各模块的仓库下，如【ukui-control-center】

3.仅wayland模式下发现的issue，提交时标题带有【wayland】，可切换X下对比确认

4.平板模式发现的issue，标题带有【平板模式】

5.在issue中提交具体的复现手法（测试工具）及硬件机型



### 风险评估

测试主机设备及硬件设备覆盖型号无法确保完全，存在未覆盖型号无法确认是否支持的风险

