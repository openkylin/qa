# 开放麒麟(openKylin)-0.9.5 版本公测-测试方案	

## 版本情况	

### 版本内容	

 1.新增UKUI4.0、分级冻结、虚拟化、新版软件商店等多个需求（详见[0.9.5版本发行说明](https://mp.weixin.qq.com/s?__biz=Mzg2MDc5MDU1OQ==&mid=2247487253&idx=1&sn=3d23029241ffe29fd4ad342f054287c3&chksm=ce204565f957cc735f8f755f7554c8c904340b45053b72b2046f279222402fb281f4d79f3f71&mpshare=1&scene=1&srcid=0112Gyix8YPoXE0QUKzaJUC9&sharer_sharetime=1673485929838&sharer_shareid=f7d3b34ddcbabc0b059530083e5853fe&exportkey=n_ChQIAhIQLJMN1wP3iEXFDJKLMPAJuhKZAgIE97dBBAEAAAAAACxqLLfKI5cAAAAOpnltbLcz9gKNyK89dVj0Lf8aj%2FrHXXi3dLfS9YnVRdpVRRK8k%2BNXG8KxCoAZnzo6LiJ2N%2F8yUKYPyWGiojkAkECP9lA2ENjn3hi0hW9LgKARPLyEWuJ85A4GPy%2F4Ci6E9fCZI00udxhG3RbjP7so2svBlg3v2YC9h0osRndPoQMUaj3dRTxReHRecowIWBTXP4FzSgqWd5JjxzV2g9KgPeTBB0fcDVzQLwmA8RyYKqrTtOENU59j5HIHupxyIrYGssoG%2Fi75Yxf7Ca7p9U5QXAKqU4gmjMfs5RSW2AdGuw5Mw46Y0FkjcB3BavolHHOQmzD%2BCvnXfqXDF2q2f2rtuE1j&acctmode=0&pass_ticket=EhWSZJzyye%2Fphy4i5yBoOmdSRsJohTLKUGVAqeOstOi1JGqBVe2fimTUOHCYZ7QguMjGYiyyOr0s4TmliVpxOA%3D%3D&wx_header=0#rd)）

 2.修复issue（详见[0.9.5版本发行说明](https://mp.weixin.qq.com/s?__biz=Mzg2MDc5MDU1OQ==&mid=2247487253&idx=1&sn=3d23029241ffe29fd4ad342f054287c3&chksm=ce204565f957cc735f8f755f7554c8c904340b45053b72b2046f279222402fb281f4d79f3f71&mpshare=1&scene=1&srcid=0112Gyix8YPoXE0QUKzaJUC9&sharer_sharetime=1673485929838&sharer_shareid=f7d3b34ddcbabc0b059530083e5853fe&exportkey=n_ChQIAhIQLJMN1wP3iEXFDJKLMPAJuhKZAgIE97dBBAEAAAAAACxqLLfKI5cAAAAOpnltbLcz9gKNyK89dVj0Lf8aj%2FrHXXi3dLfS9YnVRdpVRRK8k%2BNXG8KxCoAZnzo6LiJ2N%2F8yUKYPyWGiojkAkECP9lA2ENjn3hi0hW9LgKARPLyEWuJ85A4GPy%2F4Ci6E9fCZI00udxhG3RbjP7so2svBlg3v2YC9h0osRndPoQMUaj3dRTxReHRecowIWBTXP4FzSgqWd5JjxzV2g9KgPeTBB0fcDVzQLwmA8RyYKqrTtOENU59j5HIHupxyIrYGssoG%2Fi75Yxf7Ca7p9U5QXAKqU4gmjMfs5RSW2AdGuw5Mw46Y0FkjcB3BavolHHOQmzD%2BCvnXfqXDF2q2f2rtuE1j&acctmode=0&pass_ticket=EhWSZJzyye%2Fphy4i5yBoOmdSRsJohTLKUGVAqeOstOi1JGqBVe2fimTUOHCYZ7QguMjGYiyyOr0s4TmliVpxOA%3D%3D&wx_header=0#rd)））

## 测试计划

### 测试环境

 1.X86、RISC-V、ARM\虚拟机（Linux\win+VMware、virtualbox）

 2.开发板适配测试及特殊机型缺陷验证需要使用指定设备，其他无要求
 
 3.系统支持PC平板二合一，可在PC平板两种模式下进行验证

 4.镜像直接安装或历史镜像升级至最新版本

### 测试策略

1.版本测试，对于新增功能、回归issue的针对性测试

2.虚拟机支持：支持Linux\win+VMware、virtualbox及增强功能等


### 测试时间

1月12日--2月28日

### 测试内容

1.新增内容试用测试：UKUI4.0、分级冻结、虚拟化、新版软件商店等多个需求，详见[0.9.5版本发行说明](https://mp.weixin.qq.com/s?__biz=Mzg2MDc5MDU1OQ==&mid=2247487253&idx=1&sn=3d23029241ffe29fd4ad342f054287c3&chksm=ce204565f957cc735f8f755f7554c8c904340b45053b72b2046f279222402fb281f4d79f3f71&mpshare=1&scene=1&srcid=0112Gyix8YPoXE0QUKzaJUC9&sharer_sharetime=1673485929838&sharer_shareid=f7d3b34ddcbabc0b059530083e5853fe&exportkey=n_ChQIAhIQLJMN1wP3iEXFDJKLMPAJuhKZAgIE97dBBAEAAAAAACxqLLfKI5cAAAAOpnltbLcz9gKNyK89dVj0Lf8aj%2FrHXXi3dLfS9YnVRdpVRRK8k%2BNXG8KxCoAZnzo6LiJ2N%2F8yUKYPyWGiojkAkECP9lA2ENjn3hi0hW9LgKARPLyEWuJ85A4GPy%2F4Ci6E9fCZI00udxhG3RbjP7so2svBlg3v2YC9h0osRndPoQMUaj3dRTxReHRecowIWBTXP4FzSgqWd5JjxzV2g9KgPeTBB0fcDVzQLwmA8RyYKqrTtOENU59j5HIHupxyIrYGssoG%2Fi75Yxf7Ca7p9U5QXAKqU4gmjMfs5RSW2AdGuw5Mw46Y0FkjcB3BavolHHOQmzD%2BCvnXfqXDF2q2f2rtuE1j&acctmode=0&pass_ticket=EhWSZJzyye%2Fphy4i5yBoOmdSRsJohTLKUGVAqeOstOi1JGqBVe2fimTUOHCYZ7QguMjGYiyyOr0s4TmliVpxOA%3D%3D&wx_header=0#rd)）

2.版本已解决issue验证，重点验证issue解决及是否有新问题引入（issue列表详见[0.9.5版本发行说明](https://mp.weixin.qq.com/s?__biz=Mzg2MDc5MDU1OQ==&mid=2247487253&idx=1&sn=3d23029241ffe29fd4ad342f054287c3&chksm=ce204565f957cc735f8f755f7554c8c904340b45053b72b2046f279222402fb281f4d79f3f71&mpshare=1&scene=1&srcid=0112Gyix8YPoXE0QUKzaJUC9&sharer_sharetime=1673485929838&sharer_shareid=f7d3b34ddcbabc0b059530083e5853fe&exportkey=n_ChQIAhIQLJMN1wP3iEXFDJKLMPAJuhKZAgIE97dBBAEAAAAAACxqLLfKI5cAAAAOpnltbLcz9gKNyK89dVj0Lf8aj%2FrHXXi3dLfS9YnVRdpVRRK8k%2BNXG8KxCoAZnzo6LiJ2N%2F8yUKYPyWGiojkAkECP9lA2ENjn3hi0hW9LgKARPLyEWuJ85A4GPy%2F4Ci6E9fCZI00udxhG3RbjP7so2svBlg3v2YC9h0osRndPoQMUaj3dRTxReHRecowIWBTXP4FzSgqWd5JjxzV2g9KgPeTBB0fcDVzQLwmA8RyYKqrTtOENU59j5HIHupxyIrYGssoG%2Fi75Yxf7Ca7p9U5QXAKqU4gmjMfs5RSW2AdGuw5Mw46Y0FkjcB3BavolHHOQmzD%2BCvnXfqXDF2q2f2rtuE1j&acctmode=0&pass_ticket=EhWSZJzyye%2Fphy4i5yBoOmdSRsJohTLKUGVAqeOstOi1JGqBVe2fimTUOHCYZ7QguMjGYiyyOr0s4TmliVpxOA%3D%3D&wx_header=0#rd)）

### issue提交
规范可参考
[openKylin测试参与指南](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%89%88%E6%9C%AC%E6%B5%8B%E8%AF%95%E5%8F%82%E4%B8%8E%E6%8C%87%E5%8D%97.md) 及
[openKylin 缺陷issue处理流程](https://gitee.com/openkylin/qa/blob/master/openKylin%E7%BC%BA%E9%99%B7%E5%A4%84%E7%90%86%E6%B5%81%E7%A8%8B.md)

1.所有[已知issue](https://gitee.com/organizations/openkylin/issues?assignee_id=&author_id=&branch=&collaborator_ids=&issue_search=&label_ids=&label_text=&milestone_id=&priority=&private_issue=&program_id=&project_id=&project_type=&scope=&single_label_id=&single_label_text=&sort=&state=progressing&target_project=) 

可见 gitee-openKylin-【任务】-“进行中”，以及QA仓库历史已知问题列表，已知缺陷不需要重复提交；

（点openkylin-任务，即可到所有issue页面，在搜索框输入标题或者ID都可以找到对应issue）

2.issue提交在各模块的仓库下，如【ukui-control-center】

3.仅wayland模式下发现的issue，提交时标题带有【wayland】，可切换X下对比确认

4.平板模式发现的issue，标题带有【平板模式】

5.在issue中提交具体的复现手法（测试工具）及硬件机型



### 风险评估

测试主机设备及硬件设备覆盖型号无法确保完全，存在未覆盖型号无法确认是否支持的风险

