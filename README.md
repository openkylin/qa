# QA SIG

致力于提升openKylin社区版本质量，包括社区版本测试、质量保障

QA主要涉及的质量保障内容：

- 制定测试参与指南及评审规则，让更多的社区开发者进行使用、问题反馈、优化及贡献
- 开发测试工具以提升代码开发效率和测试验证效率
- 运作测试相关活动，包括各版本及重要组件的测试

## 主要内容

- 测试相关文档指导
- 已知问题

## SIG成员

SIG-owner

- 唐晓东

SIG-maintainer
- liyuan_yuan(liyuanyuan@kylinos.cn)
- fanwei1203(fanwei@kylinos.cn)

## 邮件

qa@lists.openkylin.top




