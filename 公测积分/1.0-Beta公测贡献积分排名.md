## 1.0-Beta公测贡献积分排名

5月16日-6月6日测试贡献积分排名

积分计算规则见：

[测试贡献评审规则](https://gitee.com/openkylin/community/blob/master/sig/qa/%E6%B5%8B%E8%AF%95%E8%B4%A1%E7%8C%AE%E8%AF%84%E5%AE%A1%E8%A7%84%E5%88%99.md)



|缺陷创建者|gitee ID|有效issue数量|贡献值|
|---------|-----------|-------|-------|
|	gaoyanglinux	|	gaoyanglinux	|	8	|	52.5	|
|	郭斌	|	hal0946	|	7	|	43.5	|
|	Ziniap	|	ziniap	|	7	|	36	|
|	张益铭	|	githubZen	|	7	|	32	|
|	君茗	|	langxianhu	|	4	|	24.5	|
|	ShadowWarfare	|	ShadowWarfare	|	4	|	20.5	|
|	江同学	|	JiangZLY	|	3	|	19.5	|
|	chipo	|	chipo	|	3	|	19	|
|	gfdgd xi	|	gfdgd-xi	|	2	|	11	|
|	LXP	|	xiangguanyun	|	2	|	10	|
|	李 不清	|	liwenjie1231	|	1	|	7	|
|	lcxkevin	|	lcxkevin	|	1	|	6.5	|
|	trackme	|	trackme	|	1	|	6	|
|	leihenzhimu	|	leihenzhimu	|	1	|	6	|
|	rtlhq	|	rtlhq	|	1	|	5	|
|	陈乾	|	qianpou	|	1	|	4.5	|
|	**总计**	|		|	**53**	|		|
