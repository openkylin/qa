## openKylin2.0-Beta2公测贡献积分排名

6月21日-7月5日测试贡献积分排名

积分计算规则见：

[测试贡献评审规则](https://gitee.com/openkylin/qa/blob/master/openKylin-V2.0-Beta2%E7%89%88%E6%9C%AC%E5%85%AC%E6%B5%8B%E6%96%B9%E6%A1%88.md)


|缺陷创建者gitee ID|+1积分|提交积分|提交计数|总贡献值|										
|---------|-----------|-------|-------|-------|										
|	gitealinux	|	12	|	131.5	|	23	|	143.5	|
|	trackme	|	2	|	76	|	15	|	78	|
|	fuweitao	|		|	25	|	5	|	25	|
|	githubZen	|		|	14	|	3	|	14	|
|	hanxuaiztt	|		|	5	|	1	|	5	|
|	yitheo	|		|	5	|	1	|	5	|
|	jelly-swiftie	|		|	3	|	1	|	3	|
|	mu_ruichao	|		|	3	|	1	|	3	
|	**总计**	|		|		|	**50**	|		|

